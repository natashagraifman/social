package com.example.graifman.socialapp.social;

import com.example.graifman.socialapp.entity.SocialEntity;
import com.example.graifman.socialapp.entity.SocialListEntity;
import com.example.graifman.socialapp.network.api.SocialApi;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SocialPresenter {

    private SocialView socialView;
    private List<SocialEntity> socialList = new ArrayList<>();
    SocialListEntity socialListEntity;

    SocialPresenter(SocialView socialView){
        this.socialView = socialView;
    }

    void updateList(){
        final SocialApi socialApi = SocialApi.getInstance();
        socialView.showLoading();
        socialApi.getSocial().enqueue(new Callback<SocialListEntity>() {
            @Override
            public void onResponse(Call<SocialListEntity> call, Response<SocialListEntity> response) {
                socialListEntity = response.body();
                if(socialListEntity != null){
                    socialView.updateList(socialListEntity.getSocial());
                } else{
                    socialView.showMessage("Falha no login");
                }
                socialView.hideLoading();
            }

            @Override
            public void onFailure(Call<SocialListEntity> call, Throwable t) {
                socialView.hideLoading();
                socialView.showMessage("Falha no acesso ao servidor");
            }
        });
    }

    public void saveMovies() {
        String jsonMoviesEntity = new Gson().toJson(socialListEntity);
        /////////moviesView.showMessage(jsonMoviesEntity);
        //socialView.saveMovieSharedPreferences(jsonMoviesEntity);
    }
}
