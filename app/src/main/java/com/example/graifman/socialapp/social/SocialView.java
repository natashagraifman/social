package com.example.graifman.socialapp.social;

import com.example.graifman.socialapp.entity.SocialEntity;

import java.util.List;

/**
 * Created by rangel on 12/16/17.
 */

public interface SocialView {

    void showLoading();
    void updateList(List<SocialEntity> movieList);
    void showMessage(String msg);
    void hideLoading();

}
