package com.example.graifman.socialapp.entity;

/**
 * Created by rangel on 12/16/17.
 */

public class SocialEntity {
    private long id;
    private String nome;
    private String imagemUrl;
    private String descricao;
    private String site;

    public long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getImagemUrl() {
        return imagemUrl;
    }

    public String getDescricao() {
        return descricao;
    }

}
